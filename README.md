# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Filled and unfilled shape                                  | 1~5%     | Y         |
| Rainbow gradient pen                            | 1~5%     | Y         |

---
### 成果圖
![](https://i.imgur.com/2LD6Zp1.png)


### How to use 
***
第一列圖形工具列是功能按鍵列，當鼠標經過按鍵顏色會變深，按下後即改變功能，不同功能畫布上鼠標有所不同，進入網頁時的預設為畫筆功能。按鍵功能由左至右分別代表：

**基本功能**
  + pen：實現畫筆功能
  + eraser：實現橡皮擦功能，顏色為透明，筆刷、圖片、文字都能擦去
  + text：會跳出文字框供使用者輸入，文字框出現後按一下文字框來輸入，輸入完按Enter即可呈現在畫布上
  + clean：清空畫布至全白，圖片、筆刷都會被去除
 
**進階功能**
  + download：按下後直接下載png檔，圖片範圍為整個畫布
  + upload：可貼上電腦裡任意大小的圖，畫布會自動調整至上傳圖片大小且固定為圖片大小
  + undo：按下即撤回最近動作，可按多次且不限於基本功能，清空也能回復
  + redo：按下即回復最近撤回動作，可按多次且不限於基本功能，清空也能回復
  + circle(shape)：可拖移圓形至滿意的大小再畫上畫布
  + triangle(shape)：可拖移三角形至滿意大小再畫上畫布，當鼠標當前位置高於mousedown位置時，會變成倒三角形
  + rectangle(shape)：可拖移矩形至滿意大小再畫上畫布

**額外功能**
  + filledOrNot：初始設定為填滿顏色的狀態，上面的形狀筆刷都會填滿內部；按下後會改變工具列圖形變成真空，形狀筆刷則空心，狀態可互相轉換
  + rainbow：和畫筆功能相同，但顏色會隨畫筆移動動態改變，使用時圖形工具會改變成彩色，如果不使用則變回pen功能
***
第二列工具列是性質調整列，前三項為下拉式選單，只要點到選單，便會跳出選項，點選後即改變，colorPick只要點下顏色按鍵，便會跳出額外的顏色選單功能。由左至右分別代表：
+  LineWidth：調整筆刷粗細，會影響到pen,eraser,rainbow的線條粗細與shape的框線粗細，預設值為10
+  TextSize：調整字體大小，會影響到text輸入後呈現的字，預設值為14
+  TextFace：調整字體樣式，會影響到text輸入後呈現的字，預設值為Arial
+  colorPick：調整筆刷顏色，會影響到pen,eraser,rainbow的線條顏色與shape的框線或填滿顏色，預設值為black
***
:::warning
:bulb: **建議**：用電腦全螢幕來實做會是最好的狀態，雖然畫布被壓縮的情況下功能仍可使用，但畫布大小固定，下載圖片時隱藏部分會是透明的哦！
:::
### How to use video example
兩者影片內容一致，只是預防任意一方影片無法載入，擇一觀看即可！（vimeo畫質較優）
#### vimeo
{%vimeo 407487358 %}
#### youtube
{%youtube 0N_-E5xWCeo %}

### Function description——Basic components
在此簡單介紹如何實做，可能會省略部分比較繁瑣內容，重點講最主要的函式
在開始實做所有功能時，要先訂好canvas上的座標以及點擊狀況
```javascript=101
//起始點座標
let x1= 0
let y1= 0

// 終點座標
let x2= 0
let y2= 0

//檢查點擊狀況
let painting=false;
```
```javascript=225
canvas.addEventListener("mousedown",startPosition);
canvas.addEventListener("mouseup",finishedPosition);
canvas.addEventListener("mousemove",draw);
```
> 監聽在canvas畫布上，滑鼠的狀態，如果按下就跳到startPosition()，放開滑鼠就跳到inishedPosition()，移動的時候就跳到draw()
> 
<<<<<<< HEAD
#### Basic control tools-pen
=======
#### ++Basic control tools-pen++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7

```javascript=112
function startPosition(e){
    painting=true;
    x1 = e.offsetX
    y1 = e.offsetY
    if(mode=="pen"||mode=="rainbow") draw(e);
    //......(程式碼省略)......//
    }
```
> 只要按下時，x1就變成當時鼠標x位置，y1則是鼠標y位置，並把painting設為true，代表正在畫畫，如果是pen模式就會直接跳到draw()函式
```java=170
function draw(e){
    if(!painting) return;

    ctx.lineWidth = paintwidth;
    ctx.lineCap = 'round'
    ctx.lineJoin = 'round'
    
    ctx.strokeStyle=colorPick;
    
    x2 = e.offsetX
    y2 = e.offsetY

    if(mode=="pen"||mode=="rainbow"){
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
        if(mode=="rainbow"){
<<<<<<< HEAD
            rainbowChange;
=======
            rainbowChange++;
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
            if(rainbowChange==360) rainbowChange=0;
        }
    }
    //......(程式碼省略)......//
    x1 = x2
    y1 = y2
    
}
```
> startPosition呼叫後會跳到此處，且鼠標在canvas上移動時也會不斷呼叫此函式。
> 如果是在painting==true的狀態才會畫上去，false時不做任何事。
> 
> 移動時x2就變成現在鼠標x位置，y2則是現在鼠標y位置。因為x1,y1還記錄上一次呼叫draw()時的位置，我就用函式將x1,y1-x2,y2連接後再將線條stroke(畫上畫布)，這樣就可以做到不間斷地畫線，做到我們需要的pen功能
> >（rainbow在後面額外功能會講解。）
```javascript=141
function finishedPosition(){
    painting=false;
    ctx.beginPath();
    //......(程式碼省略)......//
}
```
> 鼠標放開即完成當前想畫的線條，paintng狀況要設成false避免鼠標移動時繼續畫
***
<<<<<<< HEAD
#### Basic control tools-eraser
=======
#### ++Basic control tools-eraser++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
當我切換到eraser mode的時候，和pen做的事情很類似，只是要將畫筆顏色變成擦去，所以大部分用到的函式是相同的，所以相同功能不再重複說明
```javascript=112
function startPosition(e){
    painting=true;
    x1 = e.offsetX
    y1 = e.offsetY

    if(mode=="pen"||mode=="rainbow") draw(e);
    else if(mode=="eraser"){
        ctx.clearRect(x1,y1,paintwidth,paintwidth);
    }
    //......(程式碼省略)......//
}
```
> 如果滑鼠輕點，這裡eraser會清掉一塊從x1,y1為左上角，paintwidth（筆刷粗細變數）為長寬的方塊狀空白

但我想要實做的是類似筆刷那樣圓頭而且不會斷掉的線條，如果用clearRect，快速滑動時，會出現很多小方塊清除顏色，但沒有連在一起，並非我要的效果。
```javascript=170
function draw(e){
    if(!painting) return;
    //......(程式碼省略)......//
    
    else if(mode=="eraser"){
        erasermode();
    }
```
> 只要畫畫時是eraser mode就會呼叫erasermode()

```javascript=230
function erasermode(){
    　　//找到四個角
        var asin = paintwidth*Math.sin(Math.atan((y2-y1)/(x2-x1)));
        var acos = paintwidth*Math.cos(Math.atan((y2-y1)/(x2-x1)))
        var x3 = x1+asin;
        var y3 = y1-acos;
        var x4 = x1-asin;
        var y4 = y1+acos;
        var x5 = x2+asin;
        var y5 = y2-acos;
        var x6 = x2-asin;
        var y6 = y2+acos;
        
　　　　//連接
        ctx.save()
        ctx.beginPath()
        ctx.arc(x2,y2,paintwidth,0,2*Math.PI);
        ctx.clip()
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
    
　　　　//清除
        ctx.save()
        ctx.beginPath()
        ctx.moveTo(x3,y3);
        ctx.lineTo(x5,y5);
        ctx.lineTo(x6,y6);
        ctx.lineTo(x4,y4);
        ctx.closePath();
        ctx.clip()
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
}
```
> 想法和pen實現很像，我要將兩個方塊連接起來，上面那八個變數，只是在做標出兩個圓點間用長方形連起來所需的四個點。
> 
> 連接時arc()實做把想要筆刷是圓形的功能，但這個函式無法清除圓圈內物件
> clip()指只有在arc圓圈內的部分保留，未來繪製圖形超出將不顯示
> clearRect()可以清除方框內物件，又被clip()影響變成arc()形狀
> 
> 接著就是將兩個圓圈連接，中間的長方形一樣比照上面方法去除，只是arc()形狀變成長方形這樣的closePath()
***
<<<<<<< HEAD
#### Basic control tools-color selector
=======
#### ++Basic control tools-color selector++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
html
```htmlmixed=64
<input type="color" id="colorPick" name="colorPicker"/>
```
javascript
```javascript=290
document.getElementById('colorPick').addEventListener('input',pick);
function pick()
{
    console.log("pick")
    colorPick=document.getElementById('colorPick').value;
}
```
> 這裡我用了內建的color picker功能，如果點擊換顏色的方框，在javascript內會監聽到，只把我設的colorPick（筆刷顏色變數）改成點擊後get到的value即可改變筆刷顏色
***
<<<<<<< HEAD
#### Basic control tools-simple menu
=======
#### ++Basic control tools-simple menu++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
html
```htmlmixed=34
<form>
    <select name="LineWidth" id="LineWidth" onchange="changewidth();">
        <option value="10">LineWidth</option>
        <option value="10">10</option>
        <option value="15">15</option>
        <option value="20">20</option>
        <option value="25">25</option>
        <option value="30">30</option>
        <option value="35">35</option>
        <option value="40">40</option>
        <option value="45">45</option>
		</select>
	</form>
    <form>
	<select name="Textsize" id="Textsize" onchange="changeTextSize();">
		<option value="14">Textsize</option>
		<option value="18">18</option>
		<option value="24">24</option>
        <option value="30">30</option>
        <option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		</select>
	</form>
	<form>
	<select name="Textface" id="Textface" onchange="changeTextFace();">
		<option value="Arial">Arial</option>
		<option value="Arial Black">Arial Black</option>
		<option value="Courier New">Courier New</option>
		<option value="標楷體">標楷體</option>
		</select>
	</form>
```
javascript
```javascript=277
//實現換性質
function changewidth(){
    paintwidth=document.getElementById("LineWidth").value;
}
function changeTextSize(){
    textsize=document.getElementById("Textsize").value;
    console.log("textsize");
}
function changeTextFace(){
    textface=document.getElementById("Textface").value;
    console.log("textface");
}
```
> 我在html建立了三個下拉式選單，每個選項都有自己的value，我只要在下拉式選單點擊我想要的筆刷大小或字型大小或字體，javascript就會監聽到，並將變數都設為得到的value
> >因為變數名字即可辨認性質，我在這裡不贅述內容，source code可以看到其預設值
***
<<<<<<< HEAD
#### Text input
=======
#### ++Text input++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
```javascript=112
function startPosition(e){
    painting=true;
    x1 = e.offsetX
    y1 = e.offsetY
//......(程式碼省略)......//
    else if(mode=="text"){
        if(!textFlag){
            textFlag = true
            tmpx=x1;
            tmpy=y1;
            textBox.style.left = (x1+240) + 'px';
            textBox.style.top = (y1+110) + 'px';
            textBox.style['z-index'] = 6;
        }
    }
}
```
> 當我在text mode按下鼠標，就會製作出一個文字框(textBox)，並將textFlag狀態改成true，tmpx,tmpy記錄現在想要放的文字位置。
```javascript=322
//text按enter完成
function myFunction(event) {
        var x = event.key;
        var y=event.which;
        if(y==13){
            if(mode=="text"){
                if(textFlag){
                    textContent = textBox.value;
                    textFlag = false;
                    textBox.style['z-index'] = -1;
                    textBox.value = "";
                    textinging(tmpx, tmpy);
                }
     }
    }
}
```
> 我實時監聽鍵盤狀態，如果鍵盤按下Enter，代表輸入完成，將textFlag狀態改成false，並將文字框隱藏起來且清空，並呼叫textinging(tmpx, tmpy)
> >style['z-index']為-1時，會藏在網頁的圖層裡面，在css裡面實做，這裡不贅述。
```javascript=298
//實做text
function textinging(x,y){

        ctx.fillStyle = "black"; 

        ctx.save();
        ctx.beginPath();

        // 寫字
        ctx.font = textsize+"px "+textface;

        console.log(ctx.font);
        ctx.fillText(textContent, x, y); 
        ctx.restore();
        ctx.closePath();

        //......(程式碼省略)......//
    }
```
> 這裡字體顏色設為黑色，如果想改變顏色也可以設為colorPick
> 寫字時的textsize,textface就是我在下拉式選單可以改變的部分
> 呼叫canvas填文字的函式即可完成
***
<<<<<<< HEAD
#### Cursor icon
=======
#### ++Cursor icon++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
```javascript=24
canvas.style['cursor']="url(http://cur.cursors-4u.net/cursors/cur-8/cur762.cur),help";
```
> canvas內的鼠標我預設為pen模式的鼠標，因為預設模式也是pen


```javascript=32
function changemode(num){
    preMode=mode;
    mode=document.getElementById(num).id;
    console.log(mode);
    if(mode=="pen"){
        canvas.style['cursor']="url(http://cur.cursors-4u.net/cursors/cur-8/cur762.cur),help";
    }
    else if(mode=="eraser"){
        canvas.style['cursor']="url(http://www.rw-designer.com/cursor-extern.php?id=72976),help";
    }
    
    else if(mode=="text"){
        canvas.style['cursor']="text";
    }
    //......(程式碼省略)......//
    else if(mode=="circle"){
        canvas.style['cursor']="url(http://www.rw-designer.com/cursor-extern.php?id=6874),pointer";
    }
    else if(mode=="rectangle"){
        canvas.style['cursor']="url(http://www.rw-designer.com/cursor-extern.php?id=96311),pointer";
    }
    else if(mode=="triangle"){
        canvas.style['cursor']="url(http://cur.cursors-4u.net/symbols/sym-7/sym777.cur),pointer";
    }
    //......(程式碼省略)......//
}
```
> 在改變mode時直接修改canvas的鼠標，指定為網路上的鼠標圖案用url傳，如果網路上圖案出問題，則會改成後面給的javascript原生的鼠標圖案，以確保不會出錯
> 
***
<<<<<<< HEAD
#### Refresh button
=======
#### ++Refresh button++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
```javascript=264
//實做清空
function refreshmode(){
    console.log("refreshmode");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}
```
> 要清空畫布上的所有物件，只要呼叫足夠大的clearRect()

***
### Function description——Advanced tools
<<<<<<< HEAD
#### Different brush shapes
=======
#### ++Different brush shapes++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
因為圓形、三角形、長方形的實作方法非常類似，所以這裡一起講解，首先畫圖時也類似基本功能，需要在畫布上點選與移動，所以我一樣去監聽滑鼠狀態，進入到不同function。

一開始實做圖形筆刷時，能夠在鼠標放開時畫出圖形，但我們想要實做的是可以在拉動時就看到圖形大小改變，所以我做了以下function來實現：
```javascript=398
function takeSnapShot()
{
    snapshot=ctx.getImageData(0,0,canvas.width,canvas.height);
}
function restoreSnapShot()
{
    ctx.putImageData(snapshot, 0, 0);
}
```
> takeSnapShot()會把現在畫布上的狀態以圖片存到snapshot這個變數上
> restoreSnapShot()則是會snapshot存的圖片放上去
```javascript=112
function startPosition(e){
    painting=true;
    x1 = e.offsetX
    y1 = e.offsetY
    //......(程式碼省略)......//
    
    else if(mode=="circle"||mode=="rectangle"||mode=="triangle"){
        tmpx=x1;
        tmpy=y1;
        takeSnapShot();
        x2=tmpx;
        y2=tmpy;
    }
}
```
> 點下鼠標，用tmpx,tmpy紀錄此時位置以作為拉動圖形時的原點，後呼叫takeSnapShot()
> 此時takeSnapShot()存的圖是沒有任何圖形拉動的狀態
```javascript=170
function draw(e){
    if(!painting) return;

    ctx.lineWidth = paintwidth;
    ctx.lineCap = 'round'
    ctx.lineJoin = 'round'
    
    ctx.strokeStyle=colorPick;

    x2 = e.offsetX
    y2 = e.offsetY
    //......(程式碼省略)......//

    else if(mode=="circle"){
        restoreSnapShot();
        drawCircle();
    }
    else if(mode=="rectangle"){
        restoreSnapShot();
        drawRectangle();
    }
    else if(mode=="triangle"){
        restoreSnapShot();
        drawTriangle();
    }
    x1 = x2
    y1 = y2
    
}
```
> 當鼠標移動或放開時，都會不斷呼叫restoreSnapShot()，把空白時的圖片put上來。
> 再呼叫畫畫函式，不斷快速重複畫了、變成空白，就能做到實時把圖形大小畫上去的樣子
> >（finishedPosition做法相同這裡不放上來程式）
```javascript=406
//圓形實作
function drawCircle()
{
    ctx.beginPath()
    ctx.arc(tmpx,tmpy,Math.sqrt(Math.pow(x2-tmpx,2)+Math.pow(y2-tmpy,2)), 0, 2*Math.PI);
    if(fillornot){
        ctx.fillStyle=colorPick;
        ctx.fill();
    }
    else ctx.stroke();
}
//長方形實作
function drawRectangle()
{
    ctx.beginPath()
    console.log(fillornot)
    if(fillornot) {
        ctx.fillStyle=colorPick;
        ctx.fillRect(tmpx,tmpy,x2-tmpx,y2-tmpy);
    }
    else ctx.rect(tmpx,tmpy,x2-tmpx,y2-tmpy);
    ctx.stroke();
}
//三角形實作
function drawTriangle()
{
    ctx.beginPath()
    ctx.moveTo(tmpx, tmpy);
    ctx.lineTo(x2,y2);
    ctx.lineTo(2*tmpx-x2,y2);
    ctx.closePath();
    if(fillornot){
        ctx.fillStyle=colorPick;
        ctx.fill();
    }
    else ctx.stroke();
}
```
>三圖形筆刷只有畫的方式不同，其他過程類似，fillornot功能在額外部分會講解
>圓形與方形都有canvas本來的函式可以呼叫，這裡就只講三角形
>我設計三角形都是等邊，當初鼠標點選處為頂點，用線畫出底邊的兩個點再連接
>所以如果鼠標移動到頂點以上，會變成倒三角形
***
<<<<<<< HEAD
#### Un/Re-do button
=======
#### ++Un/Re-do button++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
想法是把每個動作做完的畫布狀況都存到一個array裡，如果要undo或redo時，只要調出array裡的資料放上畫布即可
```javascript=17
var canvasHistory = new Array();
canvasHistory.push(canvas.toDataURL());
```
> 製作出一個array，且在最一開始先放完全空白的畫布狀態上去
```javascript=141
function finishedPosition(){
    painting=false;
    ctx.beginPath();

    //保存歷史紀錄的圖
    if(mode!="text"){
    step++;
    console.log(step);
    if (step < canvasHistory.length) { canvasHistory.length = step; }
    var dataURL = canvas.toDataURL();
    canvasHistory.push(dataURL);
    }
        //......(程式碼省略)......//

}
```
> 每次鼠標放開代表這個動作結束，所以將此時狀態存到canvasHistory裡面，每次放進去就要將array的長度增加。
> canvas.toDataURL()可以回傳圖像和參數設置特定格式的 data URIs
> 放圖片進去的方法就是呼叫toDataURL()，來得到url傳進array裡
> 
> 除去text mode的原因是滑鼠放開時，只是呼叫出文字框，此時文字尚未畫上畫布，所以會存不到有文字的圖，在textinging函式裡我才去存圖到array，做法類似，這邊就不再放上來，可以去source code裡面看
> 
> 我也有做清空仍然可以redo,undo的功能，因為refresh時並不會在畫布中有滑鼠放開的狀態，所以我在refreshmode函式裡特地去存圖，做法也類似，這邊就不再放上來，可以去source code裡面看
```javascript=365
//把canvasHistory中存的圖(url)載下來放上去即實作undo/redo
function undoMode()
{
    console.log(step);
    if(step > 0){
        step--;
        let canvasPic = new Image();
        canvasPic.src = canvasHistory[step];
        console.log(canvasPic.width);

        canvasPic.onload = function () { 
            canvas.width = canvasPic.width;
            canvas.height = canvasPic.height;
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
    console.log(step);
}
function redoMode()
{
    if(step < canvasHistory.length - 1){
        step++;
        let canvasPic = new Image();
        canvasPic.src = canvasHistory[step];
        canvasPic.onload = function () {
            canvas.width = canvasPic.width;
            canvas.height = canvasPic.height; 
            ctx.drawImage(canvasPic, 0, 0);
        }
    }

}
```
> undo創造了一個新圖片變數叫canvasPic，要把它的src設為我們存進canvasHistory的url，所以就將step-1，提取前一次動作時的圖片url
> 
> redo步驟相同，只是將step+1，提取後一次動作時的圖片url
> 
> 如果已經沒有上一步或下一步，就不會做任何事
***
<<<<<<< HEAD
#### Image tool
=======
#### ++Image tool++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
html
```htmlmixed=18
<label class="button" id="upload" onclick="changemode(this.id)"><img src="img/upload.png" width="50" height="50">
	<input type="file" id="imageUpload" name="imageUpload"/>
</label>
```
> 如果沒有用label包起來，會出現html自帶的下載檔案按鍵
> 用圖片包起來就可以做到點選圖片即下載，達到美化功能
> 
javascript
```javascript=16
imageUpload.addEventListener('change', handleImage, false);
```
> 監聽是否有圖片上傳
```javascript=352
function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}
```
> 被呼叫後會創造一個FileReader去接收檔案，當接進來後img去接收target.files[0]，target.files[0]內容就是圖片，img的src就是target.files[0]的result
> 
> 確定檔案圖片都載好後，就將圖片畫上畫布，且改變畫布大小至圖片大小
***
<<<<<<< HEAD
#### Download
=======
#### ++Download++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
```javascript=32
function changemode(num){
//......(程式碼省略)......//

else if(mode=="save"){
        savePhoto();
        mode=preMode;
    }
}
```
> 只要點了下載的圖示，mode改變就會呼叫savePhoto()
```javascript=339
//存圖
function savePhoto()
{
    let imgUrl = canvas.toDataURL('image/png');
    let saveA = document.createElement('a');
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = 'canvas_pic'+(new Date).getTime();
    saveA.target = '_blank';
    saveA.click();
}
```
> canvas.toDataURL()可以回傳圖像和參數設置特定格式的 data URIs，設定為png
> createElement可以建立saveA成為tag a 的元素
> 所以saveA就變成圖片連結
> 設定download後的圖片名稱為canvas_pic加上時間，如果沒有加上時間就會命名為canvas_pic，當下載多次圖形，下載名稱會變成canvas_pic(次數)，使用者比較難辨認


***
### Function description——Other useful widgets

<<<<<<< HEAD
#### Filled and unfilled shape
=======
#### ++Filled and unfilled shape++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
```javascript=21
var fillornot=true;
```
> fillornot變數決定shape內部是否填滿，一開始預設值為填滿顏色的狀態，所以給定true
```javascript=76
else if(mode=="fill"){
        mode=preMode;
        if(fillornot==true){
            document.getElementById(num).src="img/circle.png";
            fillornot=false;
        }
        else{
            document.getElementById(num).src="img/fill.png";
            fillornot=true;
        }
    }
```
> 此段程式在changemode函式內，點擊按鍵mode會接收按鍵id，fillOrNot按鍵id是"fill"。進入此段程式，如果fillornot變數為true，代表此時要從填滿變成空心，所以要把工具列圖片換成空心圓，fillornot變數為false；反之再按一次按鍵就會進入else區塊，從空心轉成填滿
```javascript=410
function drawCircle()
{
    ctx.beginPath()
    //console.log(tmpx);
    //console.log(tmpy);
    ctx.arc(tmpx,tmpy,Math.sqrt(Math.pow(x2-tmpx,2)+Math.pow(y2-tmpy,2)), 0, 2*Math.PI);
    if(fillornot){
        ctx.fillStyle=colorPick;
        ctx.fill();
    }
    else ctx.stroke();
}
```
> 三圖形作法類似，這裡以圓形為例，填滿與空心作法只有在畫下時不同，如果要填滿，呼叫canvas fill()函式，就會自動填滿，不過函式預設顏色是黑色，要額外告知此時顏色

***

<<<<<<< HEAD
#### Rainbow gradient pen
=======
#### ++Rainbow gradient pen++
>>>>>>> c5742e648e265d6ce45894a2d43b7d42a9a27ef7
```javascript=22
var rainbowornot=false;
var rainbowChange=0;
```
> rainbowornot，決定是否使用此功能，一開始預設值為不用，所以給定false
> rainbowChange，顏色改變量，在之後的函式會用到，一開始不用改變設為0

```javascript==87
else if(mode=="rainbow"){
        if(rainbowornot==false){
            document.getElementById(num).src="img/rainbow.png";
            rainbowornot=true;
            canvas.style['cursor']="url(http://cur.cursors-4u.net/food/foo-6/foo515.cur),help";
        }
        else{
            document.getElementById(num).src="img/rainbownot.png";
            rainbowornot=false;
            changemode("pen");
        }
    }
```
> 在changemode函式內，性質跟Filled and unfilled shape第二段程式相似
> 如果點擊黑白彩虹，代表要實作此功能，圖片變成彩色，rainbowornot改成true，
> 因此功能會應用到畫布上，改變畫布鼠標成彩虹圖案，如果圖片失效，會變成help鼠標。
> 如果不想繼續此功能，改掉工具列圖片與rainbowornot之外，這裏預設會變回pen模式
```javascript=170
function draw(e){
    if(!painting) return;
    ctx.lineWidth = paintwidth;
    ctx.lineCap = 'round'
    ctx.lineJoin = 'round'
    ctx.strokeStyle=colorPick;
    if(mode=="rainbow"){
        ctx.strokeStyle="hsl("+rainbowChange+",59%,89%)"
    }
    
    if(mode=="pen"||mode=="rainbow"){
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
        if(mode=="rainbow"){
            rainbowChange++;
            if(rainbowChange==360) rainbowChange=0;
        }
    }
    //......(程式碼省略)......//
```
> 此段程式在draw函式裡面，draw的實作請參考Function description—Basic components
> 此功能畫筆的除了顏色以外性質都與pen功能相同
> 如果mode檢測到是rainbow，將顏色改為hsl(rainbowChange的數值,59%,89%)
>>hsl三數值分別是色相、飽和度、亮度，其中色相是用度數(0~360)表示，類似色環

>作畫時因為會不斷呼叫draw()，每次都將rainbowChange的數值增加，就可以動態呈現在畫布上，如果超過360度會調回0度
>
***
### Gitlab page link

https://107030003.gitlab.io/AS_01_WebCanvas/

### Others (Optional)

非常謝謝助教！助教辛苦了～～

<style>
table th{
    width: 100%;
}
</style>
