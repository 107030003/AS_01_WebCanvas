//canvas DOM
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
console.log("hello");
var mode="pen";
var preMode="pen";
var paintwidth=10;
var textsize=14;
var textface="Arial";
var textFlag = false;
var textContent = "";
var colorPick="black";
var imageUpload = document.getElementById('imageUpload');
let undo = document.getElementById('undo');
let redo = document.getElementById('redo');
imageUpload.addEventListener('change', handleImage, false);
var canvasHistory = new Array();
canvasHistory.push(canvas.toDataURL());
var step = 0;
var snapshot;
var fillornot=true;
var rainbowornot=false;
var rainbowChange=0;
canvas.style['cursor']="url(http://cur.cursors-4u.net/cursors/cur-8/cur762.cur),help";

//記下輸入text的位置
var tmpx=0;
var tmpy=0;



function changemode(num){
    preMode=mode;
    mode=document.getElementById(num).id;
    console.log(mode);
    if(mode=="pen"){
        canvas.style['cursor']="url(http://cur.cursors-4u.net/cursors/cur-8/cur762.cur),help";
    }
    else if(mode=="eraser"){
        canvas.style['cursor']="url(http://www.rw-designer.com/cursor-extern.php?id=72976),help";
    }
    else if(mode=="refresh"){
        refreshmode();
        //canvas.style['cursor']="default";
        mode=preMode;
    }
    else if(mode=="text"){
        canvas.style['cursor']="text";
    }
    else if(mode=="save"){
        savePhoto();
        mode=preMode;
    }
    else if(mode=="upload")
    {
        mode=preMode;
        
    }
    else if(mode=="undo"){
        undoMode();
        mode=preMode;
    }
    else if(mode=="redo"){
        redoMode();
        mode=preMode;
    }
    else if(mode=="circle"){
        canvas.style['cursor']="url(http://www.rw-designer.com/cursor-extern.php?id=6874),pointer";
    }
    else if(mode=="rectangle"){
        canvas.style['cursor']="url(http://www.rw-designer.com/cursor-extern.php?id=96311),pointer";
    }
    else if(mode=="triangle"){
        canvas.style['cursor']="url(http://cur.cursors-4u.net/symbols/sym-7/sym777.cur),pointer";
    }
    else if(mode=="fill"){
        mode=preMode;
        if(fillornot==true){
            document.getElementById(num).src="img/circle.png";
            fillornot=false;
        }
        else{
            document.getElementById(num).src="img/fill.png";
            fillornot=true;
        }
    }
    else if(mode=="rainbow"){
        if(rainbowornot==false){
            document.getElementById(num).src="img/rainbow.png";
            rainbowornot=true;
            canvas.style['cursor']="url(http://cur.cursors-4u.net/food/foo-6/foo515.cur),help";
        }
        else{
            document.getElementById(num).src="img/rainbownot.png";
            rainbowornot=false;
            changemode("pen");
        }
    }
}

//起始點座標
let x1= 0
let y1= 0

// 終點座標
let x2= 0
let y2= 0

//檢查點擊狀況
let painting=false;

function startPosition(e){
    painting=true;
    x1 = e.offsetX
    y1 = e.offsetY

   

    if(mode=="pen"||mode=="rainbow") draw(e);
    else if(mode=="eraser"){
        ctx.clearRect(x1,y1,paintwidth,paintwidth);
    }
    else if(mode=="text"){
        if(!textFlag){
            textFlag = true
            tmpx=x1;
            tmpy=y1;
            textBox.style.left = (x1+240) + 'px';
            textBox.style.top = (y1+110) + 'px';
            textBox.style['z-index'] = 6;
        }
    }
    else if(mode=="circle"||mode=="rectangle"||mode=="triangle"){
        tmpx=x1;
        tmpy=y1;
        takeSnapShot();
        x2=tmpx;
        y2=tmpy;
    }
}
function finishedPosition(){
    painting=false;
    ctx.beginPath();

    //保存歷史紀錄的圖
    if(mode!="text"){
    step++;
    console.log(step);
    if (step < canvasHistory.length) { canvasHistory.length = step; }
    var dataURL = canvas.toDataURL();
    canvasHistory.push(dataURL);
    }
    if(mode=="circle")
    {
        restoreSnapShot();
        drawCircle();
    }
    else if(mode=="rectangle")
    {
        restoreSnapShot();
        drawRectangle();
    }
    else if(mode=="triangle")
    {
        restoreSnapShot();
        drawTriangle();
    }

}
function draw(e){
    if(!painting) return;

    ctx.lineWidth = paintwidth;
    ctx.lineCap = 'round'
    ctx.lineJoin = 'round'
    
    ctx.strokeStyle=colorPick;
    if(mode=="rainbow"){
        ctx.strokeStyle="hsl("+rainbowChange+",59%,89%)"
    }
    
    x2 = e.offsetX
    y2 = e.offsetY

    if(mode=="pen"||mode=="rainbow"){
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
        if(mode=="rainbow"){
            rainbowChange++;
            if(rainbowChange==360) rainbowChange=0;
        }
    }
    else if(mode=="eraser"){
        erasermode();
    }
    else if(mode=="circle"){
        restoreSnapShot();
        drawCircle();
    }
    else if(mode=="rectangle"){
        restoreSnapShot();
        drawRectangle();
    }
    else if(mode=="triangle"){
        restoreSnapShot();
        drawTriangle();
    }
    x1 = x2
    y1 = y2
    
}

//聽滑鼠事件
canvas.addEventListener("mousedown",startPosition);
canvas.addEventListener("mouseup",finishedPosition);
canvas.addEventListener("mousemove",draw);

//實做橡皮擦
function erasermode(){
    　　//找到四個角
        var asin = paintwidth*Math.sin(Math.atan((y2-y1)/(x2-x1)));
        var acos = paintwidth*Math.cos(Math.atan((y2-y1)/(x2-x1)))
        var x3 = x1+asin;
        var y3 = y1-acos;
        var x4 = x1-asin;
        var y4 = y1+acos;
        var x5 = x2+asin;
        var y5 = y2-acos;
        var x6 = x2-asin;
        var y6 = y2+acos;
        
　　　　//保持連貫
        ctx.save()
        ctx.beginPath()
        ctx.arc(x2,y2,paintwidth,0,2*Math.PI);
        ctx.clip()
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
    
　　　　//清除像素
        ctx.save()
        ctx.beginPath()
        ctx.moveTo(x3,y3);
        ctx.lineTo(x5,y5);
        ctx.lineTo(x6,y6);
        ctx.lineTo(x4,y4);
        ctx.closePath();
        ctx.clip()
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
}

//實做清空
function refreshmode(){
    console.log("refreshmode");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    //存清空的圖
    step++;
        console.log(step);
        if (step < canvasHistory.length) { canvasHistory.length = step; }
        var dataURL = canvas.toDataURL();
        canvasHistory.push(dataURL);
}

//實現換性質
function changewidth(){
    paintwidth=document.getElementById("LineWidth").value;
}
function changeTextSize(){
    textsize=document.getElementById("Textsize").value;
    console.log("textsize");
}
function changeTextFace(){
    textface=document.getElementById("Textface").value;
    console.log("textface");
}
//換顏色
document.getElementById('colorPick').addEventListener('input',pick);
function pick()
{
    console.log("pick")
    colorPick=document.getElementById('colorPick').value;
}


//實做text
function textinging(x,y){

        ctx.fillStyle = "black"; 

        ctx.save();
        ctx.beginPath();

        // 寫字
        ctx.font = textsize+"px "+textface;

        console.log(ctx.font);
        ctx.fillText(textContent, x, y); 
        ctx.restore();
        ctx.closePath();

        //保存已經打上去的圖（如果在點擊時做，圖上還沒有文字）
        step++;
        console.log(step);
        if (step < canvasHistory.length) { canvasHistory.length = step; }
        var dataURL = canvas.toDataURL();
        canvasHistory.push(dataURL);
    }

//text按enter完成
function myFunction(event) {
        var x = event.key;
        var y=event.which;
        if(y==13){
            if(mode=="text"){
                if(textFlag){
                    textContent = textBox.value;
                    textFlag = false;
                    textBox.style['z-index'] = -1;
                    textBox.value = "";
                    textinging(tmpx, tmpy);
                }
     }
    }
}

//存圖
function savePhoto()
{
    let imgUrl = canvas.toDataURL('image/png');
    let saveA = document.createElement('a');
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = 'canvas_pic'+(new Date).getTime();
    saveA.target = '_blank';
    saveA.click();
}

//上傳圖片
function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}
//把canvasHistory中存的圖(url)載下來放上去即實作undo/redo
function undoMode()
{
    console.log(step);
    if(step > 0){
        step--;
        let canvasPic = new Image();
        canvasPic.src = canvasHistory[step];
        console.log(canvasPic.width);

        canvasPic.onload = function () { 
            canvas.width = canvasPic.width;
            canvas.height = canvasPic.height;
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
    console.log(step);
}
function redoMode()
{
    if(step < canvasHistory.length - 1){
        step++;
        let canvasPic = new Image();
        canvasPic.src = canvasHistory[step];
        canvasPic.onload = function () {
            canvas.width = canvasPic.width;
            canvas.height = canvasPic.height; 
            ctx.drawImage(canvasPic, 0, 0);
        }
        // redo.classList.add('active');
    }

}
function takeSnapShot()
{
    snapshot=ctx.getImageData(0,0,canvas.width,canvas.height);
}
function restoreSnapShot()
{
    ctx.putImageData(snapshot, 0, 0);
}
//圓形實作
function drawCircle()
{
    ctx.beginPath()
    //console.log(tmpx);
    //console.log(tmpy);
    ctx.arc(tmpx,tmpy,Math.sqrt(Math.pow(x2-tmpx,2)+Math.pow(y2-tmpy,2)), 0, 2*Math.PI);
    if(fillornot){
        ctx.fillStyle=colorPick;
        ctx.fill();
    }
    else ctx.stroke();
}
//長方形實作
function drawRectangle()
{
    ctx.beginPath()
    //console.log(tmpx);
    //console.log(tmpy);
    console.log(fillornot)
    if(fillornot) {
        ctx.fillStyle=colorPick;
        ctx.fillRect(tmpx,tmpy,x2-tmpx,y2-tmpy);
    }
    else ctx.rect(tmpx,tmpy,x2-tmpx,y2-tmpy);
    ctx.stroke();
}
//三角形實作
function drawTriangle()
{
    ctx.beginPath()
    //console.log(tmpx);
    //console.log(tmpy);
    ctx.moveTo(tmpx, tmpy);
    ctx.lineTo(x2,y2);
    ctx.lineTo(2*tmpx-x2,y2);
    ctx.closePath();
    if(fillornot){
        ctx.fillStyle=colorPick;
        ctx.fill();
    }
    else ctx.stroke();
}